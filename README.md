## Copyright

Copyright (c) 2008 Alexander Færøy <ahf@exherbo.org>
You may redistribute this package under the same terms as Vim itself.

Based in part upon gentoo-syntax, which is:
Copyright (c) 2004-2005 Ciaran McCreesh, Aaron Walker

## Installation

If you are on an Exherbo-based system:

  cave resolve app-vim/exheres-syntax

If you are on a Gentoo-based system:

  cave resolve app-vim/exheres-syntax

    or

  emerge app-vim/exheres-syntax

Other:

  Just extract the tarball to your ~/.vim/ directory.

## Bugs

If you discover any bugs in exheres-syntax, please file a bug on our
[Gitlab instance](https://gitlab.exherbo.org/exherbo-misc/exheres-syntax/-/issues).

## Authors

- Alexander Færøy <ahf@exherbo.org>
- Ingmar Vanhassel <ingmar@exherbo.org>
- Saleem Abdulrasool <compnerd@compnerd.org>
