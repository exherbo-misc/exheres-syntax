DATE = $(shell date +%Y%m%d)

all: exheres-syntax-$(DATE).tar.xz

clean:
	@echo "Cleaning ..."
	@rm -rf *.tar.xz *.tar || true

exheres-syntax-$(DATE).tar.xz:
	@echo "Generating $@ ..."
	@git archive --prefix=exheres-syntax-$(DATE)/ HEAD | xz -z > $@

upload: exheres-syntax-$(DATE).tar.xz
	@echo "Uploading $< ..."
	scp $< dev.exherbo.org:/srv/www/dev.exherbo.org/distfiles/exheres-syntax/

.default: all

.phony: clean upload
